﻿namespace StreamHighs
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.chooseVideoLabel2 = new System.Windows.Forms.Label();
            this.chooseVideoLabel1 = new System.Windows.Forms.Label();
            this.DownloadIcon = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DownloadIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chooseVideoLabel2);
            this.panel1.Controls.Add(this.chooseVideoLabel1);
            this.panel1.Controls.Add(this.DownloadIcon);
            this.panel1.ForeColor = System.Drawing.Color.Purple;
            this.panel1.Location = new System.Drawing.Point(34, 25);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2);
            this.panel1.Size = new System.Drawing.Size(565, 300);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint_1);
            // 
            // chooseVideoLabel2
            // 
            this.chooseVideoLabel2.AutoSize = true;
            this.chooseVideoLabel2.Font = new System.Drawing.Font("Georgia", 16F);
            this.chooseVideoLabel2.ForeColor = System.Drawing.Color.White;
            this.chooseVideoLabel2.Location = new System.Drawing.Point(268, 209);
            this.chooseVideoLabel2.Name = "chooseVideoLabel2";
            this.chooseVideoLabel2.Size = new System.Drawing.Size(150, 27);
            this.chooseVideoLabel2.TabIndex = 2;
            this.chooseVideoLabel2.Text = "or drag it here";
            // 
            // chooseVideoLabel1
            // 
            this.chooseVideoLabel1.AutoSize = true;
            this.chooseVideoLabel1.Font = new System.Drawing.Font("Georgia", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseVideoLabel1.Location = new System.Drawing.Point(131, 210);
            this.chooseVideoLabel1.Name = "chooseVideoLabel1";
            this.chooseVideoLabel1.Size = new System.Drawing.Size(142, 27);
            this.chooseVideoLabel1.TabIndex = 1;
            this.chooseVideoLabel1.Text = "Choose a file";
            this.chooseVideoLabel1.Click += new System.EventHandler(this.chooseVideoLabel1_Click);
            // 
            // DownloadIcon
            // 
            this.DownloadIcon.BackgroundImage = global::StreamHighs.Properties.Resources.DownloadIcon;
            this.DownloadIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DownloadIcon.Location = new System.Drawing.Point(196, 36);
            this.DownloadIcon.Name = "DownloadIcon";
            this.DownloadIcon.Size = new System.Drawing.Size(168, 153);
            this.DownloadIcon.TabIndex = 0;
            this.DownloadIcon.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(634, 361);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StreamHighs";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DownloadIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label chooseVideoLabel2;
        private System.Windows.Forms.Label chooseVideoLabel1;
        private System.Windows.Forms.PictureBox DownloadIcon;
    }
}

