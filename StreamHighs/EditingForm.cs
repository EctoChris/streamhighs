﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;

namespace StreamHighs
{
    public partial class EditingForm : Form
    {
        public Bitmap rawImage;
        public Image<Bgr, byte> rawImageBGR;
        public EditingForm(VideoCapture videoFile)
        {
            InitializeComponent();
            //VideoManager.testEmgu(videoFile, displayArea, displayArea2);
            //VideoManager.readTextFromImage(displayArea, displayArea2);
            rawImage = new Bitmap(StreamHighs.Properties.Resources.EliminatedTextDetection);
            rawImageBGR =  new Image<Bgr, byte>(rawImage);
        }

        private void erosionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rawImageBGR = rawImageBGR.Erode(1);
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void dilationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rawImageBGR = rawImageBGR.Dilate(1);
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
            rawImageBGR = rawImageBGR.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Open, kernel , new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rawImageBGR = new Image<Bgr, byte>(rawImage);
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
            rawImageBGR = rawImageBGR.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Close, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void gradientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
            rawImageBGR = rawImageBGR.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Gradient, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void topHatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
            rawImageBGR = rawImageBGR.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Tophat, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void blackHatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
            rawImageBGR = rawImageBGR.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Blackhat, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void binarizationThresholdingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image<Gray, byte> greyImage = rawImageBGR.Convert<Gray, byte>();
            Image<Gray, byte> imgBinarize = new Image<Gray, byte>(greyImage.Width, greyImage.Height, new Gray(0));
            CvInvoke.Threshold(greyImage, imgBinarize, 50, 255, Emgu.CV.CvEnum.ThresholdType.Otsu);
            rawImageBGR = imgBinarize.Convert<Bgr, byte>();
            displayArea2.Image = rawImageBGR.Bitmap;
        }

        private void readTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
          //  VideoManager.tessDetectText(rawImageBGR);
        }
    }
}
