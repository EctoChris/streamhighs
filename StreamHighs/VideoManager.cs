﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.OCR;
using System.Drawing;
using System.Reflection;
using Tesseract;

//TODO:
//Make variables public within VideoManager (timestamps)
//Modularize highlight algorithm so it happens at completion of highlight finding

namespace StreamHighs
{
    public class VideoManager
    {
        public static string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\";
        public static TesseractEngine tess = new TesseractEngine("./tessdata", "eng", EngineMode.TesseractAndCube);
        public static int frameNo = 0;
        public static double totalFrames;
        public static double fps;
        public static List<int> timeStamps = new List<int>();
        public static bool finishedCapturingTimeStamps = false;
        public static int totalTimeOfVideo;
        public static string desktopPath;
        public static VideoWriter videoWriter;
        public static VideoCapture video;
        public static string videoName;
        public static string videoPath;
        public static double videoFrameWidth;
        public static double videoFrameHeight;
        public static double centerRegionWidthMin, centerRegionWidthMax, centerRegionHeightMin, centerRegionHeightMax, centerRegionXLocationMin, centerRegionXLocationMax, centerRegionYLocationMin, centerRegionYLocationMax;
       
        public static bool detectTextCenterOfFortniteImage(Image<Bgr, byte> img, PictureBox displayArea2)
        {
            //Step 1: Sobel = EdgeDetection in OpenCV
            //Thresholding = pixels within range of 0 - 255
            Image<Gray, byte> sobel = img.Convert<Gray, byte>().Sobel(1, 0, 3).AbsDiff(new Gray(0.0)).Convert<Gray, byte>().ThresholdBinary(new Gray(100), new Gray(255));
            //Step 2: Dilation = an operation that 'grows' or 'thickens' objects in an image
            Mat SE = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new System.Drawing.Size(10, 1), new System.Drawing.Point(-1, -1));
            sobel = sobel.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Dilate, SE, new System.Drawing.Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Reflect, new MCvScalar(255));

            //Step 3: Find Contours (Contours = object 'boundaries')
            Emgu.CV.Util.VectorOfVectorOfPoint contours = new Emgu.CV.Util.VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(sobel, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < contours.Size; i++)
            {
                Rectangle boundRect = CvInvoke.BoundingRectangle(contours[i]);
                // if (boundRect.Width > 70 && boundRect.Width < 171 && boundRect.Height > 11 && boundRect.Height < 20 && boundRect.Location.X > 470 && boundRect.Location.X < 800 && boundRect.Location.Y > 490 && boundRect.Location.Y < 500) 
                //if (boundRect.Width > 70 && boundRect.Width < 171 && boundRect.Height > 8 & boundRect.Height < 25 && boundRect.Location.X > 400 && boundRect.Location.X < 900 && boundRect.Location.Y > 480 && boundRect.Location.Y < 550) 
                //{
                if(isWithinCenter(boundRect))
                    return true;
                //}
            }
            return false;
        }


        public static int MyRound(double d)
        {
            if (d < 0)
            {
                return (int)(d - 0.5);
            }
            return (int)(d + 0.5);
        }

        public static void DetectText(Image<Bgr, byte> img, PictureBox displayArea, PictureBox displayArea2, VideoCapture video, double fps)
        {
            //Step 1: Sobel = EdgeDetection in OpenCV
            //Thresholding = pixels within range of 0 - 255
            Image<Gray, byte> sobel = img.Convert<Gray, byte>().Sobel(1, 0, 3).AbsDiff(new Gray(0.0)).Convert<Gray, byte>().ThresholdBinary(new Gray(100), new Gray(255));
            //Step 2: Dilation = an operation that 'grows' or 'thickens' objects in an image
            Mat SE = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new System.Drawing.Size(10, 1), new System.Drawing.Point(-1, -1));
            sobel = sobel.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Dilate, SE, new System.Drawing.Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Reflect, new MCvScalar(255));

            //Step 3: Find Contours (Contours = object 'boundaries')
            Emgu.CV.Util.VectorOfVectorOfPoint contours = new Emgu.CV.Util.VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(sobel, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);

            //Step 4: Define Geometrical Constraints:
            List<Rectangle> list = new List<Rectangle>(); //=================OUTPUT CODE

            //Step 4: Add each timeStamp to list:

            //Foreach object,check if contours with conditions to see if satisfied.
            //If satisfied, add to list of object boundaries
            for (int i = 0; i < contours.Size; i++)
            {
                Rectangle boundRect = CvInvoke.BoundingRectangle(contours[i]);
                //double aspectRatio = boundRect.Width / boundRect.Height;
               // if (aspectRatio > 2 && boundRect.Width > 20 && boundRect.Height > 7 && boundRect.Height < 200)
                // if (aspectRatio > 2 && boundRect.Width > 20 && boundRect.Height > 7 && boundRect.Height < 200)
              //   if (boundRect.Width > 70 && boundRect.Width < 171 && boundRect.Height > 11 && boundRect.Height < 20 && boundRect.Location.X > 470 && boundRect.Location.X < 800 && boundRect.Location.Y > 490 && boundRect.Location.Y < 500)
                if (isWithinCenter(boundRect))
                {
                    list.Add(boundRect); //=================OUTPUT CODE
                    // Console.WriteLine("Found Highlight: boundRectWidth = " + boundRect.Width.ToString() + " boundRectHeight = " + boundRect.Height.ToString() + " boundRect.Location.X = " + boundRect.Location.X.ToString() + " boundRect.Location.Y = " + boundRect.Location.Y.ToString());
                    double timeNum = video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames);
                    double timeStamp = timeNum / fps;
                    int timeStampInt = (int)timeStamp;
                    timeStamps.Add(timeStampInt);
                    Console.WriteLine("Highlight: " + timeStampInt.ToString() + " many seconds in");
                }
            }

            //    //============================== OUTPUT CODE ===================================================================//

            Image<Bgr, byte> imgOut = img.CopyBlank();

            ////Testing BoundRects
            MCvScalar red = new MCvScalar(0, 255, 255);
            //Rectangle region = new Rectangle(500, 495, 150, 20);
            Rectangle region = new Rectangle(VideoManager.MyRound(centerRegionXLocationMin), MyRound(centerRegionYLocationMin), MyRound(centerRegionWidthMax), MyRound(centerRegionHeightMax));
            CvInvoke.Rectangle(imgOut, region, red, -1);

            foreach (var r in list)
            {
                //  CvInvoke.Rectangle(img, r, new MCvScalar(0, 0, 255), 2);
                CvInvoke.Rectangle(imgOut, r, new MCvScalar(0, 255, 255), -1);
            }

            imgOut._And(img);
            displayArea.Image = img.Bitmap;
            displayArea2.Image = imgOut.Bitmap;
            //imgOut = imagePreProcessingForTess(imgOut);
            //  //  tessDetectText(imgOut);
        }

        private static bool isWithinCenter(Rectangle rect)
        {
            //Text region conditions
            //If width is valid
            if (rect.Width > centerRegionWidthMin && rect.Width < centerRegionWidthMax)
            {
                //If height is valid
                if (rect.Height > centerRegionHeightMin && rect.Height < centerRegionHeightMax)
                {
                    //If xLocation is valid
                    if (rect.Location.X > centerRegionXLocationMin && rect.Location.X < centerRegionXLocationMax)
                    {
                        //If yLocation is valid
                        if (rect.Location.Y > centerRegionYLocationMin && rect.Location.Y < centerRegionYLocationMax)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public static async void testEmgu(VideoCapture video, PictureBox displayArea, PictureBox displayArea2)
        {
            try
            {
                totalFrames = video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
                fps = video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);
                totalTimeOfVideo = (int)(totalFrames / fps);
                //   desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\" + tempString;
                desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                while (!finishedCapturingTimeStamps)
                {
                    Mat m = new Mat();

                   // frameNo += 70;
                    frameNo++;
                    video.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, frameNo);
                    video.Read(m);
                    if (!m.IsEmpty)
                    {
                        displayArea.Image = m.Bitmap;
                        DetectText(m.ToImage<Bgr, byte>(), displayArea, displayArea2, video, fps);
                        // await Task.Delay(1000 / Convert.ToInt32(fps));
                        // await Task.Delay(200 / Convert.ToInt32(fps));
                        // await Task.Delay(10);
                        await Task.Delay(20);
                    }
                    else
                    {
                       // FindHighlights();
                        return;
                    }

                    double timeNum = video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames);
                    double timeStamp = timeNum / fps;
                    int timeStampInt = (int)timeStamp;
                    if (timeStampInt > totalTimeOfVideo)
                    {
                        //FindHighlights();
                        return;
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static Bitmap ConvertByteToImage(byte[] bytes)
        {
            return (new Bitmap(Image.FromStream(new MemoryStream(bytes))));
        }

        public static byte[] DecodeHex(string hextext)
        {
            String[] arr = hextext.Split('-');
            byte[] array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                array[i] = Convert.ToByte(arr[i], 16);
            return array;
        }

        public static byte[] ConvertImageToByte(Image My_Image)
        {
            MemoryStream m1 = new MemoryStream();
            new Bitmap(My_Image).Save(m1, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] header = new byte[] { 255, 216 };
            header = m1.ToArray();
            return (header);
        }
    }

    }

//Image<Gray, Byte> alteredImage = new Image<Gray, Byte>(testImage).Sobel(1, 0, 3).AbsDiff(new Gray(0.0)).Convert<Gray, byte>().ThresholdBinary(new Gray(50), new Gray(255));
// Image<Gray, Byte> alteredImage = new Image<Gray, Byte>(testImage).Sobel(1, 0, 3).AbsDiff(new Gray(0.0)).Convert<Gray, byte>().ThresholdBinary(new Gray(50), new Gray(255));