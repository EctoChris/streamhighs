﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;


namespace StreamHighs
{
    public partial class Main : Form
    {
        OpenFileDialog ofd = new OpenFileDialog();
        string filePath = null;
        string fileName = null;

        public Main()
        {
            InitializeComponent();
            chooseVideoLabel1.ForeColor = CustomColours.LightPurple;
            //panel1.ForeColor = ResizeRedraw;
        }


        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            //Drag and drop effect in windows
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

       

        private void textBox2_DragEnter(object sender, DragEventArgs e)
        {
            //Drag and drop effect in windows
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.panel1.ClientRectangle, CustomColours.MediumPurple, ButtonBorderStyle.Solid);

        }

        private void chooseVideoLabel1_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Mp4|*.mp4";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filePath = ofd.FileName;
                fileName = ofd.SafeFileName;

                VideoCapture rawFile = new VideoCapture(filePath);
                //MessageBox.Show("File Name = " + ofd.FileName);
                MainForm main = new MainForm(rawFile, fileName, filePath);
                this.Hide();
                main.Show();
            }
        }
    }
}
