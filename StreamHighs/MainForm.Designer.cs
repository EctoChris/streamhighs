﻿namespace StreamHighs
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleBar = new System.Windows.Forms.Panel();
            this.streamHighsTitle = new System.Windows.Forms.Label();
            this.videoPropertiesLabel = new System.Windows.Forms.Label();
            this.gameSelectionLabel = new System.Windows.Forms.Label();
            this.highlightSettingsLabel = new System.Windows.Forms.Label();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.lengthLabel = new System.Windows.Forms.Label();
            this.fileNameInput = new System.Windows.Forms.Label();
            this.lengthInput = new System.Windows.Forms.Label();
            this.overwatchLogo = new System.Windows.Forms.PictureBox();
            this.fortniteLogo = new System.Windows.Forms.PictureBox();
            this.apexLogo = new System.Windows.Forms.PictureBox();
            this.LogoPictureBox = new System.Windows.Forms.PictureBox();
            this.recordDeathsLabel = new System.Windows.Forms.Label();
            this.recordKillLabel = new System.Windows.Forms.Label();
            this.killRecordLengthsLabel = new System.Windows.Forms.Label();
            this.splitRawVideoLabel = new System.Windows.Forms.Label();
            this.outputLocationLabel = new System.Windows.Forms.Label();
            this.chooseFolderButton = new System.Windows.Forms.Button();
            this.findHighlightsButton = new System.Windows.Forms.Button();
            this.splitOutputCheckBox = new StreamHighs.SHCheckbox();
            this.recordKillsCheckBox = new StreamHighs.SHCheckbox();
            this.recordDeathsCheckBox = new StreamHighs.SHCheckbox();
            this.overwatchCheckBox = new StreamHighs.SHCheckbox();
            this.fortniteCheckBox = new StreamHighs.SHCheckbox();
            this.apexCheckBox = new StreamHighs.SHCheckbox();
            this.titleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overwatchLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fortniteLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apexLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // titleBar
            // 
            this.titleBar.Controls.Add(this.streamHighsTitle);
            this.titleBar.Controls.Add(this.LogoPictureBox);
            this.titleBar.Location = new System.Drawing.Point(-5, 2);
            this.titleBar.Name = "titleBar";
            this.titleBar.Size = new System.Drawing.Size(640, 54);
            this.titleBar.TabIndex = 0;
            // 
            // streamHighsTitle
            // 
            this.streamHighsTitle.AutoSize = true;
            this.streamHighsTitle.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.streamHighsTitle.Location = new System.Drawing.Point(236, 10);
            this.streamHighsTitle.Name = "streamHighsTitle";
            this.streamHighsTitle.Size = new System.Drawing.Size(181, 35);
            this.streamHighsTitle.TabIndex = 1;
            this.streamHighsTitle.Text = "StreamHighs";
            // 
            // videoPropertiesLabel
            // 
            this.videoPropertiesLabel.AutoSize = true;
            this.videoPropertiesLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 12.25F);
            this.videoPropertiesLabel.Location = new System.Drawing.Point(0, 66);
            this.videoPropertiesLabel.Name = "videoPropertiesLabel";
            this.videoPropertiesLabel.Size = new System.Drawing.Size(151, 23);
            this.videoPropertiesLabel.TabIndex = 1;
            this.videoPropertiesLabel.Text = "Video Properties:";
            // 
            // gameSelectionLabel
            // 
            this.gameSelectionLabel.AutoSize = true;
            this.gameSelectionLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 12.25F);
            this.gameSelectionLabel.Location = new System.Drawing.Point(0, 200);
            this.gameSelectionLabel.Name = "gameSelectionLabel";
            this.gameSelectionLabel.Size = new System.Drawing.Size(143, 23);
            this.gameSelectionLabel.TabIndex = 2;
            this.gameSelectionLabel.Text = "Game Selection:";
            // 
            // highlightSettingsLabel
            // 
            this.highlightSettingsLabel.AutoSize = true;
            this.highlightSettingsLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 12.25F);
            this.highlightSettingsLabel.Location = new System.Drawing.Point(293, 66);
            this.highlightSettingsLabel.Name = "highlightSettingsLabel";
            this.highlightSettingsLabel.Size = new System.Drawing.Size(161, 23);
            this.highlightSettingsLabel.TabIndex = 3;
            this.highlightSettingsLabel.Text = "Highlight Settings:";
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.fileNameLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.fileNameLabel.Location = new System.Drawing.Point(4, 101);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(79, 20);
            this.fileNameLabel.TabIndex = 4;
            this.fileNameLabel.Text = "File Name:";
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.lengthLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lengthLabel.Location = new System.Drawing.Point(5, 134);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(59, 20);
            this.lengthLabel.TabIndex = 5;
            this.lengthLabel.Text = "Length:";
            // 
            // fileNameInput
            // 
            this.fileNameInput.AutoSize = true;
            this.fileNameInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.fileNameInput.ForeColor = System.Drawing.Color.White;
            this.fileNameInput.Location = new System.Drawing.Point(89, 101);
            this.fileNameInput.Name = "fileNameInput";
            this.fileNameInput.Size = new System.Drawing.Size(57, 20);
            this.fileNameInput.TabIndex = 6;
            this.fileNameInput.Text = "default";
            // 
            // lengthInput
            // 
            this.lengthInput.AutoSize = true;
            this.lengthInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.lengthInput.ForeColor = System.Drawing.Color.White;
            this.lengthInput.Location = new System.Drawing.Point(89, 134);
            this.lengthInput.Name = "lengthInput";
            this.lengthInput.Size = new System.Drawing.Size(57, 20);
            this.lengthInput.TabIndex = 7;
            this.lengthInput.Text = "default";
            // 
            // overwatchLogo
            // 
            this.overwatchLogo.BackgroundImage = global::StreamHighs.Properties.Resources.overwatchLogo;
            this.overwatchLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.overwatchLogo.Location = new System.Drawing.Point(27, 301);
            this.overwatchLogo.Name = "overwatchLogo";
            this.overwatchLogo.Size = new System.Drawing.Size(66, 37);
            this.overwatchLogo.TabIndex = 13;
            this.overwatchLogo.TabStop = false;
            // 
            // fortniteLogo
            // 
            this.fortniteLogo.BackgroundImage = global::StreamHighs.Properties.Resources.fortniteLogo;
            this.fortniteLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fortniteLogo.Location = new System.Drawing.Point(21, 261);
            this.fortniteLogo.Name = "fortniteLogo";
            this.fortniteLogo.Size = new System.Drawing.Size(80, 34);
            this.fortniteLogo.TabIndex = 12;
            this.fortniteLogo.TabStop = false;
            // 
            // apexLogo
            // 
            this.apexLogo.BackgroundImage = global::StreamHighs.Properties.Resources.apexLogo;
            this.apexLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.apexLogo.Location = new System.Drawing.Point(28, 228);
            this.apexLogo.Name = "apexLogo";
            this.apexLogo.Size = new System.Drawing.Size(65, 29);
            this.apexLogo.TabIndex = 11;
            this.apexLogo.TabStop = false;
            // 
            // LogoPictureBox
            // 
            this.LogoPictureBox.BackgroundImage = global::StreamHighs.Properties.Resources.StreamHighsLogo;
            this.LogoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.LogoPictureBox.Location = new System.Drawing.Point(9, -5);
            this.LogoPictureBox.Name = "LogoPictureBox";
            this.LogoPictureBox.Size = new System.Drawing.Size(69, 65);
            this.LogoPictureBox.TabIndex = 0;
            this.LogoPictureBox.TabStop = false;
            // 
            // recordDeathsLabel
            // 
            this.recordDeathsLabel.AutoSize = true;
            this.recordDeathsLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.recordDeathsLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.recordDeathsLabel.Location = new System.Drawing.Point(293, 101);
            this.recordDeathsLabel.Name = "recordDeathsLabel";
            this.recordDeathsLabel.Size = new System.Drawing.Size(110, 20);
            this.recordDeathsLabel.TabIndex = 14;
            this.recordDeathsLabel.Text = "Record Deaths:";
            // 
            // recordKillLabel
            // 
            this.recordKillLabel.AutoSize = true;
            this.recordKillLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.recordKillLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.recordKillLabel.Location = new System.Drawing.Point(293, 133);
            this.recordKillLabel.Name = "recordKillLabel";
            this.recordKillLabel.Size = new System.Drawing.Size(90, 20);
            this.recordKillLabel.TabIndex = 15;
            this.recordKillLabel.Text = "Record Kills:";
            // 
            // killRecordLengthsLabel
            // 
            this.killRecordLengthsLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.killRecordLengthsLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.killRecordLengthsLabel.Location = new System.Drawing.Point(293, 162);
            this.killRecordLengthsLabel.Name = "killRecordLengthsLabel";
            this.killRecordLengthsLabel.Size = new System.Drawing.Size(146, 44);
            this.killRecordLengthsLabel.TabIndex = 16;
            this.killRecordLengthsLabel.Text = "Length of time to record before kills:";
            // 
            // splitRawVideoLabel
            // 
            this.splitRawVideoLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.splitRawVideoLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.splitRawVideoLabel.Location = new System.Drawing.Point(293, 213);
            this.splitRawVideoLabel.Name = "splitRawVideoLabel";
            this.splitRawVideoLabel.Size = new System.Drawing.Size(146, 44);
            this.splitRawVideoLabel.TabIndex = 17;
            this.splitRawVideoLabel.Text = "Split output video into separate games";
            // 
            // outputLocationLabel
            // 
            this.outputLocationLabel.AutoSize = true;
            this.outputLocationLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.25F);
            this.outputLocationLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.outputLocationLabel.Location = new System.Drawing.Point(293, 270);
            this.outputLocationLabel.Name = "outputLocationLabel";
            this.outputLocationLabel.Size = new System.Drawing.Size(122, 20);
            this.outputLocationLabel.TabIndex = 18;
            this.outputLocationLabel.Text = "Output Location:";
            // 
            // chooseFolderButton
            // 
            this.chooseFolderButton.BackColor = System.Drawing.Color.Silver;
            this.chooseFolderButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chooseFolderButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.35F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseFolderButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.chooseFolderButton.Location = new System.Drawing.Point(434, 270);
            this.chooseFolderButton.Name = "chooseFolderButton";
            this.chooseFolderButton.Size = new System.Drawing.Size(128, 25);
            this.chooseFolderButton.TabIndex = 22;
            this.chooseFolderButton.Text = "Choose Folder ....";
            this.chooseFolderButton.UseVisualStyleBackColor = false;
            // 
            // findHighlightsButton
            // 
            this.findHighlightsButton.BackColor = System.Drawing.Color.Black;
            this.findHighlightsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.findHighlightsButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.findHighlightsButton.Location = new System.Drawing.Point(297, 310);
            this.findHighlightsButton.Name = "findHighlightsButton";
            this.findHighlightsButton.Size = new System.Drawing.Size(224, 39);
            this.findHighlightsButton.TabIndex = 23;
            this.findHighlightsButton.Text = "Find Highlights";
            this.findHighlightsButton.UseVisualStyleBackColor = false;
            this.findHighlightsButton.Click += new System.EventHandler(this.findHighlightsButton_Click);
            // 
            // splitOutputCheckBox
            // 
            this.splitOutputCheckBox.BackgroundImage = global::StreamHighs.Properties.Resources.defaultCheckBox;
            this.splitOutputCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splitOutputCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.splitOutputCheckBox.Location = new System.Drawing.Point(450, 225);
            this.splitOutputCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.splitOutputCheckBox.Name = "splitOutputCheckBox";
            this.splitOutputCheckBox.Size = new System.Drawing.Size(20, 20);
            this.splitOutputCheckBox.TabIndex = 21;
            this.splitOutputCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.splitOutputCheckBox.UseVisualStyleBackColor = true;
            // 
            // recordKillsCheckBox
            // 
            this.recordKillsCheckBox.BackgroundImage = global::StreamHighs.Properties.Resources.defaultCheckBox;
            this.recordKillsCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.recordKillsCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.recordKillsCheckBox.Location = new System.Drawing.Point(450, 135);
            this.recordKillsCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.recordKillsCheckBox.Name = "recordKillsCheckBox";
            this.recordKillsCheckBox.Size = new System.Drawing.Size(20, 20);
            this.recordKillsCheckBox.TabIndex = 20;
            this.recordKillsCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.recordKillsCheckBox.UseVisualStyleBackColor = true;
            // 
            // recordDeathsCheckBox
            // 
            this.recordDeathsCheckBox.BackgroundImage = global::StreamHighs.Properties.Resources.defaultCheckBox;
            this.recordDeathsCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.recordDeathsCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.recordDeathsCheckBox.Location = new System.Drawing.Point(450, 102);
            this.recordDeathsCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.recordDeathsCheckBox.Name = "recordDeathsCheckBox";
            this.recordDeathsCheckBox.Size = new System.Drawing.Size(20, 20);
            this.recordDeathsCheckBox.TabIndex = 19;
            this.recordDeathsCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.recordDeathsCheckBox.UseVisualStyleBackColor = true;
            // 
            // overwatchCheckBox
            // 
            this.overwatchCheckBox.BackgroundImage = global::StreamHighs.Properties.Resources.defaultCheckBox;
            this.overwatchCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.overwatchCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.overwatchCheckBox.Location = new System.Drawing.Point(131, 310);
            this.overwatchCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.overwatchCheckBox.Name = "overwatchCheckBox";
            this.overwatchCheckBox.Size = new System.Drawing.Size(20, 20);
            this.overwatchCheckBox.TabIndex = 10;
            this.overwatchCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.overwatchCheckBox.UseVisualStyleBackColor = true;
            this.overwatchCheckBox.CheckedChanged += new System.EventHandler(this.overwatchCheckBox_CheckedChanged);
            // 
            // fortniteCheckBox
            // 
            this.fortniteCheckBox.BackgroundImage = global::StreamHighs.Properties.Resources.defaultCheckBox;
            this.fortniteCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.fortniteCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fortniteCheckBox.Location = new System.Drawing.Point(131, 270);
            this.fortniteCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.fortniteCheckBox.Name = "fortniteCheckBox";
            this.fortniteCheckBox.Size = new System.Drawing.Size(20, 20);
            this.fortniteCheckBox.TabIndex = 9;
            this.fortniteCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fortniteCheckBox.UseVisualStyleBackColor = true;
            this.fortniteCheckBox.CheckedChanged += new System.EventHandler(this.fortniteCheckBox_CheckedChanged);
            // 
            // apexCheckBox
            // 
            this.apexCheckBox.BackgroundImage = global::StreamHighs.Properties.Resources.defaultCheckBox;
            this.apexCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.apexCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.apexCheckBox.Location = new System.Drawing.Point(130, 235);
            this.apexCheckBox.Margin = new System.Windows.Forms.Padding(0);
            this.apexCheckBox.Name = "apexCheckBox";
            this.apexCheckBox.Size = new System.Drawing.Size(20, 20);
            this.apexCheckBox.TabIndex = 8;
            this.apexCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.apexCheckBox.UseVisualStyleBackColor = true;
            this.apexCheckBox.CheckedChanged += new System.EventHandler(this.apexCheckBox_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(634, 361);
            this.Controls.Add(this.findHighlightsButton);
            this.Controls.Add(this.chooseFolderButton);
            this.Controls.Add(this.splitOutputCheckBox);
            this.Controls.Add(this.recordKillsCheckBox);
            this.Controls.Add(this.recordDeathsCheckBox);
            this.Controls.Add(this.outputLocationLabel);
            this.Controls.Add(this.splitRawVideoLabel);
            this.Controls.Add(this.killRecordLengthsLabel);
            this.Controls.Add(this.recordKillLabel);
            this.Controls.Add(this.recordDeathsLabel);
            this.Controls.Add(this.overwatchLogo);
            this.Controls.Add(this.fortniteLogo);
            this.Controls.Add(this.apexLogo);
            this.Controls.Add(this.overwatchCheckBox);
            this.Controls.Add(this.fortniteCheckBox);
            this.Controls.Add(this.apexCheckBox);
            this.Controls.Add(this.lengthInput);
            this.Controls.Add(this.fileNameInput);
            this.Controls.Add(this.lengthLabel);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.highlightSettingsLabel);
            this.Controls.Add(this.gameSelectionLabel);
            this.Controls.Add(this.videoPropertiesLabel);
            this.Controls.Add(this.titleBar);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StreamHighs";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.titleBar.ResumeLayout(false);
            this.titleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overwatchLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fortniteLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apexLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel titleBar;
        private System.Windows.Forms.PictureBox LogoPictureBox;
        private System.Windows.Forms.Label streamHighsTitle;
        private System.Windows.Forms.Label videoPropertiesLabel;
        private System.Windows.Forms.Label gameSelectionLabel;
        private System.Windows.Forms.Label highlightSettingsLabel;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.Label fileNameInput;
        private System.Windows.Forms.Label lengthInput;
        private SHCheckbox apexCheckBox;
        private SHCheckbox fortniteCheckBox;
        private SHCheckbox overwatchCheckBox;
        private System.Windows.Forms.PictureBox apexLogo;
        private System.Windows.Forms.PictureBox fortniteLogo;
        private System.Windows.Forms.PictureBox overwatchLogo;
        private System.Windows.Forms.Label recordDeathsLabel;
        private System.Windows.Forms.Label recordKillLabel;
        private System.Windows.Forms.Label killRecordLengthsLabel;
        private System.Windows.Forms.Label splitRawVideoLabel;
        private System.Windows.Forms.Label outputLocationLabel;
        private SHCheckbox recordDeathsCheckBox;
        private SHCheckbox recordKillsCheckBox;
        private SHCheckbox splitOutputCheckBox;
        private System.Windows.Forms.Button chooseFolderButton;
        private System.Windows.Forms.Button findHighlightsButton;
    }
}