﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StreamHighs
{
    public partial class SHCheckbox : CheckBox
    {
        PictureBox pictureBox;
        private Image Hovered => Properties.Resources.hoveredCheckBox;
        private Image Selected => Properties.Resources.selectedCheckBox;
        private Image Default => Properties.Resources.defaultCheckBox;
        int newWidth = 20;
        int newHeight = 20;
        [Category("Custom")]
        [Browsable(true)]
        [Editor(typeof(System.Windows.Forms.Design.WindowsFormsComponentEditor), typeof(System.Drawing.Design.UITypeEditor))]

        public SHCheckbox()
        {
            InitializeComponent();
            pictureBox = new PictureBox();
            this.Text = null;
            this.Controls.Add(pictureBox);
            this.pictureBox.Size = new System.Drawing.Size(newWidth, newHeight);
            this.pictureBox.BackColor = Color.Transparent;
            this.pictureBox.BackgroundImage = Default;
            this.pictureBox.Visible = true;
            this.pictureBox.BackgroundImageLayout = ImageLayout.Stretch;
            this.pictureBox.Location = new Point(0, 0);
            this.pictureBox.MouseEnter += (sender, args) =>
            {
                if(!this.Checked)
                     this.pictureBox.BackgroundImage = this.Hovered;
            };

            this.pictureBox.MouseLeave += (sender, args) =>
            {
                if (this.Checked)
                    this.pictureBox.BackgroundImage = this.Selected;
                else
                    this.pictureBox.BackgroundImage = this.Default;
            };

            //this.pictureBox.MouseClick += (sender, args) => {
            //    this.Checked = !Checked;
            //};

            this.pictureBox.Click += (sender, args) =>
            {
                this.Checked = !Checked;
            };

        }

        public override bool AutoSize
        {
            get { return base.AutoSize; }
            set { base.AutoSize = false; }
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            //Rectangle rc = new Rectangle(new Point(0, 1), new Size(newWidth, newHeight));
        }

        protected override void OnCheckStateChanged(EventArgs e)
        {
            base.OnCheckStateChanged(e);
            if (this.Checked)
            {
                this.pictureBox.BackgroundImage = Selected;
            }
            else
            {
                this.pictureBox.BackgroundImage = Default;
            }
        }

        protected override void OnMouseEnter(EventArgs eventargs)
        {
            base.OnMouseEnter(eventargs);
            this.pictureBox.MouseEnter += (sender, args) =>
            {
                this.pictureBox.BackgroundImage = Hovered;
            };
        }

        protected override void OnMouseLeave(EventArgs eventargs)
        {
            base.OnMouseLeave(eventargs);
            this.pictureBox.MouseEnter += (sender, args) =>
            {
                this.pictureBox.BackgroundImage = Default ;
            };
        }

    }
}
