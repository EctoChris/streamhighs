﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace StreamHighs
{
    public static class CustomColours
    {
        public static readonly Color DarkPurple =  Color.FromArgb(59,12,89);
        public static readonly Color MediumPurple = Color.FromArgb(97, 10, 155);
        public static readonly Color LightPurple = Color.FromArgb(153, 0, 255);
        public static readonly Color DarkGrey = Color.FromArgb(30, 30, 30);
        public static readonly Color MediumGrey = Color.FromArgb(46, 46, 46);
        public static readonly Color LightGrey = Color.FromArgb(72, 72, 72);
    }

}
