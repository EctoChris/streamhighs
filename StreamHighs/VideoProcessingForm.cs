﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.OCR;
using System.Reflection;
using System.IO;

namespace StreamHighs
{
    public partial class VideoProcessingForm : Form
    {

        public double totalFrames;
        public double fps;
        public int totalTimeOfVideo;
        public string desktopPath;
        public bool finishedCapturingTimeStamps = false;
        public int frameNo = 0;
        public List<int> timeStamps = new List<int>();
        public int LEAD_UP;
        public int POST_KILL;
        public double tempTotalTimeOfVideo;
        public int framesToSkip;
        public List<HighLight> highlights;


        public VideoProcessingForm(int LEAD_UP, int POST_KILL)
        {
            this.LEAD_UP = LEAD_UP;
            this.POST_KILL = POST_KILL;
            InitializeComponent();
            setupUI();
            calculateFramesToSkipAndFrameSize();
            analyzeFrames();
          //  VideoManager.testEmgu(VideoManager.video, displayArea, displayArea2);

        }

        private void setupUI()
        {
            titleBar.BackColor = CustomColours.MediumGrey;
            streamHighsTitle.ForeColor = CustomColours.LightPurple;
        }

        private void calculateFramesToSkipAndFrameSize()
        {
            totalFrames = VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
            fps = VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);
            totalTimeOfVideo = (int)(totalFrames / fps);
            framesToSkip = VideoManager.MyRound(fps * 2.333333333);
            VideoManager.videoFrameWidth =  VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth);
            VideoManager.videoFrameHeight = VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight);
            VideoManager.centerRegionWidthMin = 0.0546875  * VideoManager.videoFrameWidth;
            VideoManager.centerRegionWidthMax = 0.23 * VideoManager.videoFrameWidth;
            VideoManager.centerRegionHeightMin = 0.01527777778 * VideoManager.videoFrameHeight;
            VideoManager.centerRegionHeightMax = 0.035 * VideoManager.videoFrameHeight;
            VideoManager.centerRegionXLocationMin = 0.3671875 * VideoManager.videoFrameWidth;
            VideoManager.centerRegionXLocationMax = 0.625 * VideoManager.videoFrameWidth;
            VideoManager.centerRegionYLocationMin = 0.6805555556 * VideoManager.videoFrameHeight;
            VideoManager.centerRegionYLocationMax = 0.694444444444 * VideoManager.videoFrameHeight;
            Console.WriteLine("XLocationMin: " + VideoManager.centerRegionXLocationMin);
            Console.WriteLine("XLocationMax: " + VideoManager.centerRegionXLocationMax);
            Console.WriteLine("YLocationMin: " + VideoManager.centerRegionYLocationMin);
            Console.WriteLine("YLocationMax: " + VideoManager.centerRegionYLocationMax);
            Console.WriteLine("FPS: " + fps);
            Console.WriteLine("Frames to Skip: " + framesToSkip);
        }

        public async void analyzeFrames()
        {
            try
            {
                //   desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\" + tempString;
                desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\" + "Test01.mp4";
                Mat m = new Mat();

                while (true)
                {
                    frameNo += framesToSkip;
                    VideoManager.video.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, frameNo);
                    VideoManager.video.Read(m);
                    if (!m.IsEmpty)
                    {
                        // detectTextRegion(m.ToImage<Bgr, byte>());
                        // VideoManager.DetectText(m.ToImage<Bgr, byte>(), displayArea, displayArea2, VideoManager.video, fps);
                        detectTextRegion(m.ToImage<Bgr, byte>());
                        await Task.Delay(1);
                        if (frameNo > (totalFrames-2))
                        {
                            findHighlights();
                            return;
                        }
                    }
                    else
                    {
                        findHighlights();
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        public void detectTextRegion(Image<Bgr, byte> img)
        {
            if(VideoManager.detectTextCenterOfFortniteImage(img, displayArea2))
            {
                double timeNum = VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames);
                double timeStamp = timeNum / fps;
                int timeStampInt = (int)timeStamp;
                timeStamps.Add(timeStampInt);
                Console.WriteLine("Highlight: " + timeStampInt.ToString() + " many seconds in");
                displayArea.Image = img.Bitmap;
            }
        }

        public void findHighlights()
        {
            Console.WriteLine("Finding Highlights");
            highlights = new List<HighLight>();

            HighLight firstHighlight = new HighLight();
            if (timeStamps[0] - LEAD_UP < 0)
                firstHighlight.start = 0;
            else
                firstHighlight.start = timeStamps[0] - LEAD_UP;

            highlights.Add(firstHighlight);

            bool highlightingInProgress = true;

            for (int i = 1; i < timeStamps.Count; i++)
            {
                //If current timestamp overlaps with previous highlight: (do nothing):
                if ((timeStamps[i] - LEAD_UP) <= (timeStamps[i - 1] + POST_KILL))
                {
                    ////If last timestamp, add a finish:
                    //if (i == timeStamps.Count - 1)
                    //{
                    //    if (timeStamps[timeStamps.Count - 1] + POST_KILL > totalTimeOfVideo)
                    //        highlights[highlights.Count - 1].finish = totalTimeOfVideo;
                    //    else
                    //        highlights[highlights.Count - 1].finish = timeStamps[timeStamps.Count - 1] + POST_KILL;
                    //}
                }
                else
                {
                    //Finalize current highlight, it doesnt overlap
                    highlights[highlights.Count - 1].finish = timeStamps[i - 1] + POST_KILL;
                    highlightingInProgress = false;

                    //Create New Highlight:
                    HighLight newHighlight = new HighLight();
                    newHighlight.start = timeStamps[i] - LEAD_UP;
                    highlights.Add(newHighlight);
                    highlightingInProgress = true;
                }

                //If last timestamp, add a finish:
                if (i == timeStamps.Count - 1)
                {
                    if (timeStamps[timeStamps.Count - 1] + POST_KILL > totalTimeOfVideo)
                        highlights[highlights.Count - 1].finish = totalTimeOfVideo;
                    else
                        highlights[highlights.Count - 1].finish = timeStamps[timeStamps.Count - 1] + POST_KILL;
                }
            }


            foreach (var h in highlights)
            {
                Console.WriteLine("Highlight Start: " + h.start.ToString() + " Highlight Finish: " + h.finish.ToString());
                h.findStartAndFinishFrames(fps);
            }

            foreach(var h in highlights)
            {
                Console.WriteLine("Highlight StartFrame: " + h.startFrame + " Highlight FinishFrame: " + h.finishFrame);
            }

            outputHighlightReel();
        }

        public void outputHighlightReel()
        {
            int frameNo = 0;
            int fourcc = Convert.ToInt32(VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FourCC));
            int width = Convert.ToInt32(VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth));
            int height = Convert.ToInt32(VideoManager.video.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight));

            fourcc = VideoWriter.Fourcc('D', 'I', 'V', 'X');
            // VideoManager.videoWriter = new VideoWriter(desktopPath, fourcc, Convert.ToInt32(fps), new Size(width, height), true);
            //VideoManager.videoWriter = new VideoWriter(de)
            //VideoManager.videoWriter = new VideoWriter(desktopPath, CvInvoke.cc('X', 'V', 'I', 'D'), Convert.ToInt32(fps), new Size(width, height), true);

            //VideoManager.videoWriter = new VideoWriter(desktopPath, fourcc, VideoManager.MyRound(fps), new Size(width, height), true);
            VideoManager.videoWriter = new VideoWriter(desktopPath, fourcc, VideoManager.MyRound(fps), new Size(width, height), true);
            Mat m = new Mat();

            //Reset video to first frame (0):
            VideoManager.video.SetCaptureProperty(CapProp.PosFrames, 0);

            bool boringSectionRecentlyEnded = false;
            //Process Frames:
            while (frameNo <= (totalFrames-2))
            {
                if(boringSectionRecentlyEnded)
                {
                    VideoManager.video.SetCaptureProperty(CapProp.PosFrames, frameNo);
                    boringSectionRecentlyEnded = false;
                }
                if(checkIfPartOfHighlight(frameNo))
                {
                    //Read next frame:
                    VideoManager.video.Read(m);
                    VideoManager.videoWriter.Write(m);
                    Console.WriteLine("Writing Frame: " + frameNo + " into video");
                    //int capProp = (Convert.ToInt32(VideoManager.video.GetCaptureProperty(CapProp.PosFrames)));
                    //Console.WriteLine("Current Frame Position: " + capProp);
                }
                else
                {
                    Console.WriteLine("Frame: " + frameNo + " isn't in highlight");
                    if (checkIfPartOfHighlight(frameNo + 1))
                        boringSectionRecentlyEnded = true;
                }


                //if (checkIfPartOfHighlight(frameNo))
                //{
                //    try
                //    {
                //        // m = new Mat();
                //        // VideoManager.video.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, frameNo);

                //        VideoManager.video.Read(m);
                //        VideoManager.videoWriter.Write(m);
                //        Console.WriteLine("Frame " + frameNo + " IN HIGHLIGHT");
                //    } catch(Exception e)
                //    {
                //        MessageBox.Show("Error: " + e.Message);
                //    }
                //}
                //else
                //    Console.WriteLine("Frame " + frameNo + " not in higlight");

                //frameNo++;
                //m.Dispose();
                //m = new Mat();
                frameNo++;
            }          
    
            if (VideoManager.videoWriter.IsOpened)
                VideoManager.videoWriter.Dispose();

            MessageBox.Show("Completed");
        }

        public bool checkIfPartOfHighlight(int num)
        {
            foreach(var h in highlights)
            {
                if (num >= h.startFrame && num <= h.finishFrame)
                    return true;
            }
            return false;
        }

    }
}
