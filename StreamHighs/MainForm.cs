﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;

namespace StreamHighs
{
    public partial class MainForm : Form
    {

        public MainForm(VideoCapture rawFile, string fileName, string filePath)
        {
            InitializeComponent();

            VideoManager.video = rawFile;
            VideoManager.videoPath = filePath;
            VideoManager.videoName = fileName;

            this.setupUI();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void setupUI()
        {
            titleBar.BackColor = CustomColours.MediumGrey;
            streamHighsTitle.ForeColor = CustomColours.LightPurple;
            highlightSettingsLabel.ForeColor = CustomColours.LightPurple;
            gameSelectionLabel.ForeColor = CustomColours.LightPurple;
            videoPropertiesLabel.ForeColor = CustomColours.LightPurple;
            fileNameInput.Text = VideoManager.videoName;
            lengthInput.Text = "Unknown";
            findHighlightsButton.ForeColor = CustomColours.LightPurple;
        }

        private void checkBoxSwitch(int selection)
        {
            switch(selection)
            {
                case 0:
                {
                    fortniteCheckBox.Checked = false;
                    overwatchCheckBox.Checked = false;
                    break;
                }
                case 1:
                {
                    apexCheckBox.Checked = false;
                    overwatchCheckBox.Checked = false;
                    break;
                }
                case 2:
                {
                    apexCheckBox.Checked = false;
                    fortniteCheckBox.Checked = false;
                    break;
                }
                default:
                    break;
            }
        }

        private void apexCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSwitch((int)Globals.gameSelection.APEX);
        }

        private void fortniteCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSwitch((int)Globals.gameSelection.FORTNITE);
        }

        private void overwatchCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSwitch((int)Globals.gameSelection.OVERWATCH);
        }

        private void apexCheckBox_Click(object sender, EventArgs e)
        {
            apexCheckBox.Checked = !apexCheckBox.Checked;
        }

        private void findHighlightsButton_Click(object sender, EventArgs e)
        {
            //EditingForm editForm = new EditingForm(videoFile);
            VideoProcessingForm vpForm = new VideoProcessingForm(8, 2); //must change this to read from trackbar later
            this.Hide();
            vpForm.Show();
            //editForm.Show();
        }
    }
}
