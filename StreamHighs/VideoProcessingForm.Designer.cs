﻿namespace StreamHighs
{
    partial class VideoProcessingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.streamHighsTitle = new System.Windows.Forms.Label();
            this.LogoPictureBox = new System.Windows.Forms.PictureBox();
            this.titleBar = new System.Windows.Forms.Panel();
            this.displayArea = new System.Windows.Forms.PictureBox();
            this.displayArea2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
            this.titleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayArea2)).BeginInit();
            this.SuspendLayout();
            // 
            // streamHighsTitle
            // 
            this.streamHighsTitle.AutoSize = true;
            this.streamHighsTitle.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.streamHighsTitle.Location = new System.Drawing.Point(236, 10);
            this.streamHighsTitle.Name = "streamHighsTitle";
            this.streamHighsTitle.Size = new System.Drawing.Size(181, 35);
            this.streamHighsTitle.TabIndex = 1;
            this.streamHighsTitle.Text = "StreamHighs";
            // 
            // LogoPictureBox
            // 
            this.LogoPictureBox.BackgroundImage = global::StreamHighs.Properties.Resources.StreamHighsLogo;
            this.LogoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.LogoPictureBox.Location = new System.Drawing.Point(9, -5);
            this.LogoPictureBox.Name = "LogoPictureBox";
            this.LogoPictureBox.Size = new System.Drawing.Size(69, 65);
            this.LogoPictureBox.TabIndex = 0;
            this.LogoPictureBox.TabStop = false;
            // 
            // titleBar
            // 
            this.titleBar.Controls.Add(this.streamHighsTitle);
            this.titleBar.Controls.Add(this.LogoPictureBox);
            this.titleBar.Location = new System.Drawing.Point(-5, 2);
            this.titleBar.Name = "titleBar";
            this.titleBar.Size = new System.Drawing.Size(640, 54);
            this.titleBar.TabIndex = 1;
            // 
            // displayArea
            // 
            this.displayArea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.displayArea.Location = new System.Drawing.Point(4, 111);
            this.displayArea.Name = "displayArea";
            this.displayArea.Size = new System.Drawing.Size(245, 203);
            this.displayArea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.displayArea.TabIndex = 2;
            this.displayArea.TabStop = false;
            // 
            // displayArea2
            // 
            this.displayArea2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.displayArea2.Location = new System.Drawing.Point(255, 111);
            this.displayArea2.Name = "displayArea2";
            this.displayArea2.Size = new System.Drawing.Size(367, 203);
            this.displayArea2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.displayArea2.TabIndex = 3;
            this.displayArea2.TabStop = false;
            // 
            // VideoProcessingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(634, 361);
            this.Controls.Add(this.displayArea2);
            this.Controls.Add(this.displayArea);
            this.Controls.Add(this.titleBar);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "VideoProcessingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VideoProcessingForm";
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
            this.titleBar.ResumeLayout(false);
            this.titleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displayArea2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label streamHighsTitle;
        private System.Windows.Forms.PictureBox LogoPictureBox;
        private System.Windows.Forms.Panel titleBar;
        private System.Windows.Forms.PictureBox displayArea;
        private System.Windows.Forms.PictureBox displayArea2;
    }
}