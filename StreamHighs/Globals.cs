﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamHighs
{
    public static class Globals
    {
        public enum gameSelection
        {
            APEX = 0,
            FORTNITE = 1,
            OVERWATCH = 2
        };
    }

    public class HighLight
    {
        public int start;
        public int finish;
        public int startFrame;
        public int finishFrame;
        public HighLight(int start, int finish)
        {
            this.start = start;
            this.finish = finish;
        }
        public HighLight()
        {

        }

        public void findStartAndFinishFrames(double fps)
        {
            startFrame = VideoManager.MyRound(start * fps);
            finishFrame = VideoManager.MyRound(finish * fps);
        }
    }

}
